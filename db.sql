-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2019 at 05:07 PM
-- Server version: 10.3.16-MariaDB-1:10.3.16+maria~bionic
-- PHP Version: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `c_kool`
--

-- --------------------------------------------------------

--
-- Table structure for table `Company`
--

CREATE TABLE `Company` (
  `companyId` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Company`
--

INSERT INTO `Company` (`companyId`, `description`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'company A _ update', '2019-07-06 14:21:04', '2019-07-06 14:22:36', '2019-07-06 14:22:36', 3, 3, 3),
(2, 'company A', '2019-07-06 14:22:24', '2019-07-06 14:22:24', NULL, 3, NULL, NULL),
(3, 'company A', '2019-07-06 14:22:25', '2019-07-06 14:22:25', NULL, 3, NULL, NULL),
(4, 'company A', '2019-07-06 14:22:26', '2019-07-06 14:22:26', NULL, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `custId` int(11) NOT NULL,
  `custName` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `billingAddress` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `workAddress` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `taxId` varchar(30) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `branch` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tel` varchar(15) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `custGroupId` int(11) NOT NULL,
  `custTypeId` int(11) NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`custId`, `custName`, `billingAddress`, `workAddress`, `taxId`, `branch`, `contact`, `tel`, `email`, `custGroupId`, `custTypeId`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'cus_1_update', '123.2 bangkok', '84 บรม39', '110020030949', 'HeadOffice', 'k น้ำ', '0940759080', 'a@a.com', 2, 1, '2019-07-06 14:06:07', '2019-07-06 14:09:57', NULL, 3, 3, NULL),
(2, 'cus_1_deletex', '123.2 bangkok', '84 บรม39', '110020030949', 'HeadOffice', 'k น้ำ', '0940759080', 'a@a.com', 2, 1, '2019-07-06 14:10:25', '2019-07-06 14:10:50', '2019-07-06 14:10:50', 3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `CustomerGroup`
--

CREATE TABLE `CustomerGroup` (
  `custGroupId` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `disPer` decimal(10,0) NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `CustomerGroup`
--

INSERT INTO `CustomerGroup` (`custGroupId`, `description`, `disPer`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'a', '222', '2019-07-06 13:42:21', '2019-07-06 13:45:11', '2019-07-06 13:45:11', 3, 3, 3),
(2, 'a_2', '14', '2019-07-06 14:04:49', '2019-07-06 14:04:49', NULL, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `CustomerType`
--

CREATE TABLE `CustomerType` (
  `custTypeId` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `CustomerType`
--

INSERT INTO `CustomerType` (`custTypeId`, `description`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'test', '2019-07-06 13:04:08', '2019-07-06 13:04:08', NULL, NULL, NULL, NULL),
(2, 'q122213214x', '2019-07-06 13:05:12', '2019-07-06 13:25:48', '2019-07-06 13:25:48', NULL, 3, 3),
(3, 'test', '2019-07-06 13:05:43', '2019-07-06 13:05:43', NULL, NULL, NULL, NULL),
(4, 'test', '2019-07-06 13:13:43', '2019-07-06 13:13:43', NULL, NULL, NULL, NULL),
(5, 'test', '2019-07-06 13:13:57', '2019-07-06 13:13:57', NULL, NULL, NULL, NULL),
(6, 'test', '2019-07-06 13:15:16', '2019-07-06 13:15:16', NULL, NULL, NULL, NULL),
(7, 'test', '2019-07-06 13:15:25', '2019-07-06 13:15:25', NULL, NULL, NULL, NULL),
(8, 'test', '2019-07-06 13:15:31', '2019-07-06 13:15:31', NULL, NULL, NULL, NULL),
(9, 'test', '2019-07-06 13:15:51', '2019-07-06 13:15:51', NULL, NULL, NULL, NULL),
(10, 'test', '2019-07-06 13:16:10', '2019-07-06 13:16:10', NULL, NULL, NULL, NULL),
(11, 'test', '2019-07-06 13:16:18', '2019-07-06 13:16:18', NULL, NULL, NULL, NULL),
(12, 'test', '2019-07-06 13:16:37', '2019-07-06 13:16:37', NULL, NULL, NULL, NULL),
(13, 'test', '2019-07-06 13:17:37', '2019-07-06 13:17:37', NULL, NULL, NULL, NULL),
(14, 'test', '2019-07-06 13:18:20', '2019-07-06 13:18:20', NULL, 3, NULL, NULL),
(15, 'q1', '2019-07-06 13:20:50', '2019-07-06 13:20:50', NULL, 3, NULL, NULL),
(16, 'q1', '2019-07-06 13:20:54', '2019-07-06 13:20:54', NULL, 3, NULL, NULL),
(17, 'q1', '2019-07-06 13:21:08', '2019-07-06 13:21:08', NULL, 3, NULL, NULL),
(18, 'q1', '2019-07-06 13:21:25', '2019-07-06 13:21:25', NULL, 3, NULL, NULL),
(19, 'q1', '2019-07-06 13:21:26', '2019-07-06 13:21:26', NULL, 3, NULL, NULL),
(20, 'q122213214x', '2019-07-06 13:21:36', '2019-07-06 13:24:13', NULL, 3, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Material`
--

CREATE TABLE `Material` (
  `matId` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `uomId` int(11) NOT NULL,
  `sell` decimal(10,4) NOT NULL,
  `cost` decimal(10,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `alert` tinyint(1) NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lastUpdateCost` decimal(10,4) NOT NULL,
  `lastUpdateSell` decimal(10,4) NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Material`
--

INSERT INTO `Material` (`matId`, `description`, `brand`, `model`, `uomId`, `sell`, `cost`, `status`, `alert`, `url`, `lastUpdateCost`, `lastUpdateSell`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'MAT_G1 _ update', 'Sanyo', 'CX-130', 2, '300.0000', '100.0000', 1, 1, 'www.facebook.com', '0.0000', '0.0000', '2019-07-06 15:03:02', '2019-07-06 15:05:24', NULL, 3, 3, NULL),
(2, 'MAT_G1', 'Sanyo', 'CX-130', 2, '300.0000', '100.0000', 1, 1, 'www.facebook.com', '0.0000', '0.0000', '2019-07-06 15:03:28', '2019-07-06 15:06:11', '2019-07-06 15:06:11', 3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Role`
--

CREATE TABLE `Role` (
  `roleId` varchar(3) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Role`
--

INSERT INTO `Role` (`roleId`, `description`) VALUES
('10', 'sales'),
('20', 'engineer'),
('30', 'manager'),
('40', 'procurement');

-- --------------------------------------------------------

--
-- Table structure for table `Supplier`
--

CREATE TABLE `Supplier` (
  `supId` int(11) NOT NULL,
  `supName` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `taxId` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `branch` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tel` varchar(15) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `creditTerm` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `bankAccount` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `Supplier`
--

INSERT INTO `Supplier` (`supId`, `supName`, `address`, `taxId`, `branch`, `contact`, `tel`, `email`, `creditTerm`, `bankAccount`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'supName 1', 'address 1', '1220302132', 'HQ', 'MR.MARK', '0940759080', 'admin@facebook.com', '12 /2  7day on week', 'scb 110223123', '2019-07-06 14:37:11', '2019-07-06 14:37:11', NULL, 3, NULL, NULL),
(2, 'supName 1321 _ Update', 'address 1', '1220302132', 'HQ', 'MR.MARK', '0940759080', 'admin@facebook.com', '12 /2  7day on week', 'scb 110223123', '2019-07-06 14:37:28', '2019-07-06 14:37:54', NULL, 3, 3, NULL),
(3, 'supName 1321', 'address 1', '1220302132', 'HQ', 'MR.MARK', '0940759080', 'admin@facebook.com', '12 /2  7day on week', 'scb 110223123', '2019-07-06 14:38:26', '2019-07-06 14:38:30', '2019-07-06 14:38:30', 3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `UnitOfMeasure`
--

CREATE TABLE `UnitOfMeasure` (
  `uomId` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdDateTime` datetime NOT NULL,
  `updatedDateTime` datetime NOT NULL,
  `deletedDateTime` datetime DEFAULT NULL,
  `createByUserId` int(11) DEFAULT NULL,
  `updateByUserId` int(11) DEFAULT NULL,
  `deleteByUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `UnitOfMeasure`
--

INSERT INTO `UnitOfMeasure` (`uomId`, `description`, `createdDateTime`, `updatedDateTime`, `deletedDateTime`, `createByUserId`, `updateByUserId`, `deleteByUserId`) VALUES
(1, 'MT', '2019-07-01 00:00:00', '2019-07-01 00:00:00', NULL, 1, 1, NULL),
(2, 'KGs', '2019-07-01 00:00:00', '2019-07-01 00:00:00', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `userId` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `confrimPW` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `roleId` varchar(3) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`userId`, `name`, `tel`, `password`, `confrimPW`, `roleId`) VALUES
(2, 'a12342', '12345', '$2y$10$1iVczLwvT3ndIt1SNCTz..Y36xbZUYqmYSXlG6fu5ur4vzsTftsvm', '1234', '10'),
(3, 'a11', '12345', '$2y$10$TNqvreajAuQ.v/CaqrvK6uylu9wkPG32kfqqcn9RPIzQHJMY482sO', '1234', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Company`
--
ALTER TABLE `Company`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`custId`),
  ADD KEY `FK_CustomerGroup` (`custGroupId`),
  ADD KEY `FK_CustomerType` (`custTypeId`);

--
-- Indexes for table `CustomerGroup`
--
ALTER TABLE `CustomerGroup`
  ADD PRIMARY KEY (`custGroupId`);

--
-- Indexes for table `CustomerType`
--
ALTER TABLE `CustomerType`
  ADD PRIMARY KEY (`custTypeId`);

--
-- Indexes for table `Material`
--
ALTER TABLE `Material`
  ADD PRIMARY KEY (`matId`),
  ADD KEY `FK_UOM` (`uomId`);

--
-- Indexes for table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `Supplier`
--
ALTER TABLE `Supplier`
  ADD PRIMARY KEY (`supId`);

--
-- Indexes for table `UnitOfMeasure`
--
ALTER TABLE `UnitOfMeasure`
  ADD PRIMARY KEY (`uomId`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `FK_ROKE` (`roleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Company`
--
ALTER TABLE `Company`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `custId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `CustomerGroup`
--
ALTER TABLE `CustomerGroup`
  MODIFY `custGroupId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `CustomerType`
--
ALTER TABLE `CustomerType`
  MODIFY `custTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `Material`
--
ALTER TABLE `Material`
  MODIFY `matId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Supplier`
--
ALTER TABLE `Supplier`
  MODIFY `supId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `UnitOfMeasure`
--
ALTER TABLE `UnitOfMeasure`
  MODIFY `uomId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Customer`
--
ALTER TABLE `Customer`
  ADD CONSTRAINT `FK_CustomerGroup` FOREIGN KEY (`custGroupId`) REFERENCES `CustomerGroup` (`custGroupId`),
  ADD CONSTRAINT `FK_CustomerType` FOREIGN KEY (`custTypeId`) REFERENCES `CustomerType` (`custTypeId`);

--
-- Constraints for table `Material`
--
ALTER TABLE `Material`
  ADD CONSTRAINT `FK_UOM` FOREIGN KEY (`UomID`) REFERENCES `UnitOfMeasure` (`UomID`);

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `FK_ROKE` FOREIGN KEY (`RoleID`) REFERENCES `Role` (`RoleID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
