<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::POST('/signin', 'AuthController@signin');

// Route::middleware(['allow.cors'])->group(function () {     
//     Route::POST('/signin', 'AuthController@signin');
// });

Route::middleware(['auth.jwt'])->group(function () {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('customerTypes', 'CustomerTypeController');    
    Route::resource('customerGroups', 'CustomerGroupController');    
    Route::resource('customers', 'CustomerController');    
    Route::resource('companies', 'CompanyController');  
    Route::resource('suppliers', 'SupplierController');  
    Route::resource('materials', 'MaterialController');  
    
    
});


