<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model
{
    const CREATED_AT = 'createdDateTime';
    const UPDATED_AT = 'updatedDateTime';
    const DELETED_AT = 'deletedDateTime';

    public static $snakeAttributes = false;


    protected static function boot()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        DB::disableQueryLog();

        parent::boot();

        static::creating(function ($t) {
            $uid = request()->currentUserId;
            if ($uid != null) {
                $t->{'createByUserId'} =  $uid;
            }
        });

        static::updating(function ($t) {
            $uid = request()->currentUserId;
            if ($uid != null) {
                $t->{'updateByUserId'} = $uid;
            }
        });

        static::deleting(function ($t) { // before delete() method call this
            if ($t->forceDeleting) {
                //do in case of force delete
            } else {
                $uid = request()->currentUserId;
                if ($uid != null) {
                    $t->{'deleteByUserId'} = $uid;
                }
            }
        });
    }
}
