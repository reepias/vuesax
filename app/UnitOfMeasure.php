<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitOfMeasure extends BaseModel
{
    use SoftDeletes;
    protected $table = 'UnitOfMeasure';
    protected $primaryKey = 'uomId';
    public $timestamps = true;
    protected $fillable = [
        'description'
    ];    
}
