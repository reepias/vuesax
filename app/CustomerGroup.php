<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerGroup extends BaseModel
{
    use SoftDeletes;
    protected $table = 'CustomerGroup';
    protected $primaryKey = 'custGroupId';
    public $timestamps = true;

    protected $fillable = [
        'description',
        'disPer'
    ];
}
