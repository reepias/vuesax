<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends BaseModel
{
    use SoftDeletes;
    protected $table = 'Supplier';
    protected $primaryKey = 'supId';
    public $timestamps = true;

    protected $fillable = [
        'supName',
        'address',
        'taxId',
        'branch',
        'contact',
        'tel',
        'email',
        'creditTerm',
        'bankAccount'
    ];    
}
