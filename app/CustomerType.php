<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerType extends BaseModel
{
    use SoftDeletes;
    protected $table = 'CustomerType';
    protected $primaryKey = 'custTypeId';
    public $timestamps = true;

    protected $fillable = [
        'description'
    ];

    
}
