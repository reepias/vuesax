<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends BaseModel
{
    use SoftDeletes;
    protected $table = 'Company';
    protected $primaryKey = 'companyId';
    public $timestamps = true;

    protected $fillable = [
        'description'
    ];    
}
