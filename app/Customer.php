<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends BaseModel
{
    use SoftDeletes;
    protected $table = 'Customer';
    protected $primaryKey = 'custId';
    public $timestamps = true;

    protected $fillable = [
        'custName',
        'billingAddress',
        'workAddress',
        'taxId',
        'branch',
        'contact',
        'tel',
        'email',
        'custGroupId',
        'custTypeId'
    ];

    public function customerGroup(){
        return $this->hasOne('App\CustomerGroup',"custGroupId","custGroupId");
    }

    public function customerType(){
        return $this->hasOne('App\CustomerType',"custTypeId","custTypeId");
    }

}
