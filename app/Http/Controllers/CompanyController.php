<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;


class CompanyController extends Controller
{

    private $rules = [
        'description' => 'required|string|max:50,min:3'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->ok(Company::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $c = new Company($request->only('description'));
        $c->save();

        return $this->ok($c);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $c = Company::find($id);
        if ($c == null) {
            return $this->notFound();
        }
        $c->fill($request->except("companyId"));
        $c->update();
        return $this->ok($c);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Company::find($id);
        if ($c == null) {
            return $this->notFound();
        }
        $c->delete();
        $c->save();
        return $this->ok($c);
    }
}
