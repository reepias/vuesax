<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerGroup;

class CustomerGroupController extends Controller
{

    private $rules = [
        'description' => 'required|string|max:50,min:3',
        'disPer' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->ok(CustomerGroup::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $cg = new CustomerGroup($request->only('description','disPer'));
        $cg->save();

        return $this->ok($cg);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cg = CustomerGroup::find($id);
        if ($cg == null) {
            return $this->notFound();
        }
        $cg->fill($request->except("custGroupId"));
        $cg->update();
        return $this->ok($cg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cg = CustomerGroup::find($id);
        if ($cg == null) {
            return $this->notFound();
        }
        $cg->delete();
        $cg->save();
        return $this->ok($cg);
    }
}
