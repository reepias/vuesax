<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{

    private $rules = [
        'custName' => 'required|string|max:100,min:5',
        'billingAddress' => 'required',
        'workAddress' => 'required',
        'taxId' => 'required',
        'branch' => 'required|string|max:100',
        'contact' => 'required|string|max:100',
        'tel' => 'required|string|max:15',
        'email' => 'required|email',
        'custGroupId' => 'required',
        'custTypeId' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $c = Customer::all();
        $c->load('customerGroup');
        $c->load('customerType');
        return $this->ok($c);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $c = new Customer($request->except('custId'));
        $c->save();

        return $this->ok($c);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $c = Customer::find($id);
        if ($c == null) {
            return $this->notFound();
        }
        $c->fill($request->except("custId"));
        $c->update();
        return $this->ok($c);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Customer::find($id);
        if ($c == null) {
            return $this->notFound();
        }
        $c->delete();
        $c->save();
        return $this->ok($c);
    }
}
