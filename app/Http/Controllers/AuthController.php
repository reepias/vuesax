<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use \Firebase\JWT\JWT;


class AuthController extends Controller
{
    public function signin(Request $request)
    {

        $u = User::where("name", $request->input('name'))->first();

        if ($u == null) {
            return $this->errorJSON("not found user", 401);
        }
        
        if (!Hash::check($request->input('password'), $u->password)) {
            return $this->errorJSON("password not match", 401);
        }

        return $this->ok([
            "accessToken" => $this->createJWT($u)
        ]);
    }

    private function createJWT($user)
    {
        $key = env('JWT_SECRET');
        $payload = [
            "iss" =>  "C-KOOL_API_Systems",
            "aud" => $user->name,
            "iat" => time(),
            "exp" => time() + env('JWT_EXPIRE') * 60 * 60,
            "prm" => $user->roleId . '',
            "uid" => $user->userId
        ];

        return JWT::encode($payload, $key);
    }
}
