<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{


    private $rules = [
        'supName' => 'required|string|max:100,min:3',
        'address' => 'required|string|max:100,min:3',
        'taxId' => 'required|string|max:20,min:19',
        'branch'  => 'required|string|max:100,min:3',
        'contact' => 'required|string|max:100,min:3',
        'tel' => 'required|string|max:20,min:10',
        'email'  => 'required|email',
        'creditTerm' => 'required|string|max:200',
        'bankAccount' => 'required|string|max:100',
    ];



    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->ok(Supplier::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $s = new Supplier($request->all());
        $s->save();

        return $this->ok($s);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $s = Supplier::find($id);
        if ($s == null) {
            return $this->notFound();
        }
        $s->fill($request->all());
        $s->update();
        return $this->ok($s);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $s = Supplier::find($id);
        if ($s == null) {
            return $this->notFound();
        }
        $s->delete();
        $s->save();
        return $this->ok($s);
    }
}
