<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;

class MaterialController extends Controller
{


    private $rules = [
        'description' => 'required|string|max:100,min:3',
        'brand' => 'required|string|max:100,min:3',
        'uomId' => 'required',
        'sell'  => 'required|numeric',
        'cost'  => 'required|numeric',
        'status' => 'required|boolean',
        'alert' => 'required|boolean',
        'url' => 'required|string|max:100,min:3',
        'sell'  => 'required|numeric',
        'lastUpdateCost'  => 'required|numeric',
        'lastUpdateSell'  => 'required|numeric',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $m = Material::all();
        $m->load('uom');
        return $this->ok($m);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $m = new Material($request->all());
        $m->save();

        return $this->ok($m);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $m = Material::find($id);
        if ($m == null) {
            return $this->notFound();
        }
        $m->fill($request->all());
        $m->update();
        return $this->ok($m);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $m = Material::find($id);
        if ($m == null) {
            return $this->notFound();
        }
        $m->delete();
        $m->save();
        return $this->ok($m);
    }
}
