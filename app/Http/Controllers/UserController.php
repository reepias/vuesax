<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    private $rules = [
        'name' => 'required|string|max:50',
        'password' => 'required',
        'confrimPW' => 'required',
        'roleID' => 'required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userAll = User::all();
        $userAll->load("role");
        return $this->ok(
            $userAll
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestBody = $request->all();

        $validator = Validator::make($requestBody, $this->rules);
        if ($validator->fails()) {
            return $this->badRequest($validator->messages()->all());
        }

        if (User::where('name', $requestBody['name'])->count() > 0) {
            return $this->badRequest([$requestBody['name'] .  " is duplicate"]);
        }

        $u = new User();
        $u->fill($requestBody);
        $u->password =  Hash::make($u->password);
        $u->save();

        return $this->ok($u);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $u = User::find($id);
        if ($u == null) {
            return $this->notFound();
        }
        return $this->ok($u);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u = User::find($id);
        if ($u == null) {
            return $this->notFound();
        }
        $u->fill($request->except("name"));
        $u->update();
        return $this->ok($u);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $u = User::find($id);
        if ($u == null) {
            return $this->notFound();
        }
        $u->forceDelete();
        return $this->ok($u);
    }
}
