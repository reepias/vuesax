<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerType;


class CustomerTypeController extends Controller
{

    private $rules = [
        'description' => 'required|string|max:50,min:3',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->ok(CustomerType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateRequest($this->rules, $request);
        if ($errors != null) {
            return $errors;
        }

        $ct = new CustomerType($request->only('description'));
        $ct->save();

        return $this->ok($ct);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ct = CustomerType::find($id);
        if ($ct == null) {
            return $this->notFound();
        }
        $ct->fill($request->except("custTypeId"));
        $ct->update();
        return $this->ok($ct);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ct = CustomerType::find($id);
        if ($ct == null) {
            return $this->notFound();
        }
        $ct->delete();
        $ct->save();
        return $this->ok($ct);
    }
}
