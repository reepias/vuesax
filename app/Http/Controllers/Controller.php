<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function toArrayCamel()
    {
        $array = $this->toArray();

        foreach ($array as $key => $value) {
            $return[camel_case($key)] = $value;
        }

        return $return;
    }

    public function validateRequest($rules,$request){
        $requestBody = $request->all();
        $validator = Validator::make($requestBody, $rules);
        if ($validator->fails()) {
            return $this->badRequest($validator->messages()->all());
        }
        return null;
    }


    public function badRequest($errors)
    {
        return $this->errorJSON($errors, 400);
    }

    public function notFound()
    {
        return $this->errorJSON("Not Found Resouce", 404);
    }

    public function errorJSON($message, $statusCode)
    {
        return response()->json([
            "result" => NULL,
            "errorMessage" =>  is_array($message) ? $message : [$message]
        ], $statusCode);
    }


    public function ok($data)
    {
        return response()->json([
            "result" => $data,
            "errorMessage" => []
        ], 200);
    }
}
