<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use App\User;

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $token = explode(" ",$token)[1];

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'errorMessage' => ['Access Token not provided.',]
            ], 401);
        }

        try {
            $key = env('JWT_SECRET');
            $credentials = JWT::decode($token, $key, ['HS256']);
        } catch (ExpiredException $e) {
            return response()->json([
                'errorMessage' => ['Provided Access Token is expired.']
            ], 401);
        } catch (Exception $e) {
            return response()->json([
                'errorMessage' => ['An error while decoding token.' , $e->getMessage()]
            ], 401);
        }
        //$user = User::find($credentials->uid);
        $request->currentUserId = $credentials->uid;
        $request->currentUserRoleId = $credentials->prm;
        return $next($request);
    }
}
