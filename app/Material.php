<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends BaseModel
{
    use SoftDeletes;
    protected $table = 'Material';
    protected $primaryKey = 'matId';
    public $timestamps = true;

    protected $fillable = [
        'description',
        'brand',
        'model',
        'uomId',
        'sell',
        'cost',
        'status',
        'alert',
        'url',
        'lastUpdateCost' ,
        'lastUpdateSell' ,

    ];    

    public function uom(){
        return $this->hasOne('App\UnitOfMeasure',"uomId","uomId");
    }
}
