<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'User';
    protected $primaryKey = 'userId';
    public $timestamps = false;

    protected $fillable = [
        'name', 'tel', 'password', 'confrimPW','roleID'
    ];

    protected $hidden = [
        'password'
    ];


    public function role()
    {
        return $this->hasOne('App\Role',"roleId","roleId");
    }
}
