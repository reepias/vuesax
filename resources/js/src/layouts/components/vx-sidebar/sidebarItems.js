/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default [
    {
        header: "Main Menu",
        i18n: "Main Menu"
    },
    {
        url: null,
        name: "Quotaion Process",
        slug: "Quotaion Process",
        // icon: "ShoppingCartIcon",
        i18n: "Quotaion Process",
        submenu: [
            {
                url: "/quotaion/my-job",
                name: "My Job",
                slug: "My Job",
                i18n: "My Job"
            },
            {
                url: "/quotaion/create-job",
                name: "Create Job",
                slug: "Create Job",
                i18n: "Create Job"
            },
            {
                url: "/quotaion/create-bqq",
                name: "Create BQQ",
                slug: "Create BQQ",
                i18n: "Create BQQ"
            },
            {
                url: "/quotaion/job/close",
                name: "Close Job",
                slug: "Close Job",
                i18n: "Close Job"
            },
            {
                url: "/quotaion/terminate",
                name: "Terminate Quotation",
                slug: "Terminate Quotation",
                i18n: "Terminate Quotation"
            },
            {
                url: "/quotaion/approve",
                name: "Approve Quotation",
                slug: "Approve Quotation",
                i18n: "Approve Quotation"
            },
            {
                url: "/quotaion/copy",
                name: "Copy Job",
                slug: "Copy Job",
                i18n: "Copy Job"
            }
        ]
    },
    {
        url: null,
        name: "Procument",
        slug: "Procument",
        // icon: "ShoppingCartIcon",
        i18n: "Procument",
        submenu: [
            {
                url: "/procument/create-pr",
                name: "Create PR",
                slug: "Create PR",
                i18n: "Create PR"
            },
            {
                url: "/procument/approve-pr",
                name: "Approve PR",
                slug: "Approve PR",
                i18n: "Approve PR"
            },
            {
                url: "/procument/complete-po",
                name: "Complete PO",
                slug: "Complete PO",
                i18n: "Complete PO"
            },
            {
                url: "/procument/goods-receive",
                name: "Goods Receive",
                slug: "Goods Receive",
                i18n: "Goods Receive"
            },
            {
                url: "/procument/post-vendor-invoice",
                name: "Post Vendor Invoice",
                slug: "Post Vendor Invoice",
                i18n: "Post Vendor Invoice"
            },
            {
                url: "/procument/post-vendor-payment",
                name: "Print W/T Certificate",
                slug: "Print W/T Certificate",
                i18n: "Print W/T Certificate"
            },
            {
                url: "/procument/post-vendor-credit-note",
                name: "Post Vendor Credit Note",
                slug: "Post Vendor Credit Note",
                i18n: "Post Vendor Credit Note"
            },
            {
                url: "/procument/approve-payment",
                name: "Approve Payment",
                slug: "Approve Payment",
                i18n: "Approve Payment"
            },
            {
                url: "/procument/revice-edit-pr",
                name: "Review/Edit PR",
                slug: "Review/Edit PR",
                i18n: "Review/Edit PR"
            },
            {
                url: "/procument/review-revise-pr",
                name: "Review/Revise PR",
                slug: "Review/Revise PR",
                i18n: "Review/Revise PR"
            }
        ]
    },
    {
        header: "Master",
        i18n: "Master"
    },
    {
        url: "/master/customer",
        name: "Customer",
        slug: "Customer",
        i18n: "Customer"
    },
    {
        url: "/master/customer-type",
        name: "Customer Type",
        slug: "Customer Type",
        i18n: "Customer Type"
    },
    {
        url: "/master/customer-group",
        name: "Customer Group",
        slug: "Customer Group",
        i18n: "Customer Group"
    },
    {
        url: "/master/company",
        name: "Company",
        slug: "Company",
        i18n: "Company"
    },
    {
        url: "/master/material",
        name: "Material",
        slug: "Material",
        i18n: "Material"
    },
    {
        url: "/master/supplier",
        name: "Supplier",
        slug: "Supplier",
        i18n: "Supplier"
    }
];
